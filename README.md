## Requirements:
```bash
ROS2 Rolling
squaternion 0.3.2
waymo-open-dataset-tf-2-1-0==1.2.0
autoware_auto_msgs
```
#### Install Waymo Open Dataset
```bash
https://console.cloud.google.com/storage/browser/waymo_open_dataset_v_1_2_0
```
#### Install Waymo Open Dataset Python Interface
```bash
pip3 install waymo-open-dataset-tf-2-1-0==1.2.0
```

#### Install ROS2 Rolling
```bash
To build and install ROS2 Rolling, follow those instructions:
https://github.com/leo-drive/proto_to_rosbag2
```
#### Install squaternion
```bash
pip3 install squaternion
```
Use autoware_auto_msgs package that we provide. Please build this package with ROS2 Rolling.
## How to use
Populate a folder with .tfrecord files, then give the folder path as a first command line argument. The second argument is the empty ros2bag folder.
```bash
source autoware_auto_msgs/install/setup.bash
source ros2_rolling/install/setup.bash
python3 tfrecord_to_ros2bag_converter.py /home/goktug/Desktop/Waymo_Valid_Dataset/valid0/validation_validation_0000 /home/goktug/ros2bags
```
**Note that:** This script can only work with Waymo Open Dataset training or validation set because test set doesn't contain the lidar labels.

### ROS2 messages:
```bash
Topic: /camera_front | Type: sensor_msgs/msg/Image
Topic: /camera_frontleft | Type: sensor_msgs/msg/Image
Topic: /camera_frontright | Type: sensor_msgs/msg/Image 
Topic: /camera_info_cam_front | Type: sensor_msgs/msg/CameraInfo 
Topic: /camera_info_cam_frontleft | Type: sensor_msgs/msg/CameraInfo 
Topic: /camera_info_cam_frontright | Type: sensor_msgs/msg/CameraInfo 
Topic: /camera_info_cam_sideleft | Type: sensor_msgs/msg/CameraInfo 
Topic: /camera_info_cam_sideright | Type: sensor_msgs/msg/CameraInfo 
Topic: /camera_sideleft | Type: sensor_msgs/msg/Image 
Topic: /camera_sideright | Type: sensor_msgs/msg/Image 
Topic: /cloud_front | Type: sensor_msgs/msg/PointCloud2 
Topic: /cloud_left | Type: sensor_msgs/msg/PointCloud2 
Topic: /cloud_rear | Type: sensor_msgs/msg/PointCloud2 
Topic: /cloud_right | Type: sensor_msgs/msg/PointCloud2 
Topic: /cloud_top | Type: sensor_msgs/msg/PointCloud2 
Topic: /detected_objects | Type: autoware_auto_msgs/msg/DetectedObjects 
Topic: /lidar_labels | Type: visualization_msgs/msg/MarkerArray 
Topic: /odometry | Type: nav_msgs/msg/Odometry 
Topic: /tf | Type: tf2_msgs/msg/TFMessage
```

### Odometry message details:
```bash
Header frame id: Map
Child frame id: Vehicle
PoseWithCovariance:
  - Position: Exist
  - Orientation: Exist
TwistWithCovariance:
  - Linear velocity: Exist
  - Angular velocity: Doesn't exist
```
**Note that:** Waymo Open Datset does not contain the linear velocity in the map frame. It was provided using Kalman filter.

### Detected Object message details:
```bash
Detection frame: Vehicle
Polygon Points: 4 points at each corner of the 3D bounding box
Types: Car, pedestrian, cyclist
Kinematics:
  - Pose: Exist
  - Pose Covariance: Doesn't exist
  - Twist: Doesn't exist
  - Twist Covariance: Doesn't exist
```

#### TF Tree:
```bash
- Map
  - Vehicle
    - cam_front
    - cam_frontleft
    - cam_frontright
    - cam_sideleft
    - cam_sideright
    - lid_front
    - lid_left
    - lid_rear
    - lid_right
    - lid_top
```